module.exports = function(config) {
  config.set({
    preprocessors: {
      'app/tests.webpack.js': ['webpack', 'sourcemap']
    },

    reporters: ['spec'],

    files: [
      'app/tests.webpack.js'
    ],

    singleRun: false,

    frameworks: ['jasmine'],

    browsers: ['Chrome'],

    webpack: require('./webpack.config'),

    // Hide webpack build information from output
    webpackMiddleware: {
      noInfo: 'errors-only'
    }
  });
};