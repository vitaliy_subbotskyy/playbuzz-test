# AngularJS test app for PlayBuzz


### Installing Dependencies

```
npm install
```

### Building sources

```
npm run build
```

### Running the Application

- Run `npm start`.
- The app should be opened in your default browser. If not, navigate your browser to [http://localhost:8080/](http://localhost:8080/) to see the application 
  running.


### Unit Testing

Jasmine and Karma are used for unit tests.

- Start Karma with `npm test`.
- Unit tests are running in Chrome only by default.
- To run Karma in watch mode `npm run tdd`



## Task description

####Scope
- Build a basic product that displays video items feed from various sources
- Potential sources are: Youtube, Facebook and a given URL
- To get feed items use the following live feed-service endpoint:
https://cdn.playbuzz.com/content/feed/items

####UI
- Simple nav-bar with ‘VIDEO FEED’ title
- Each feed item contains 3 elements: item-title, item-views, video
- When a certain item is missing data, show the relevant errors instead of the video:
‘Youtube video is missing’, ‘Facebook video post is missing’,..