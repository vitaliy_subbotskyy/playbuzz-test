import './pb-title.module';

export default angular.module('pbTitle')
  .component('pbTitle', {
    template: 'VIDEO FEED'
  });