import '../../core';
import '../title';
import '../feedItems/facebookItem';
import '../feedItems/html5VideoItem';
import '../feedItems/youtubeItem';

angular.module('pbVideoFeed',
    ['core', 'pbYoutubeItem', 'pbFacebookItem', 'pbVideoItem']
);