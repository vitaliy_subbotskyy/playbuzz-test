import 'to-string-loader';
import './pb-video-feed.module';

export default angular.
  module('pbVideoFeed').
  component('pbVideoFeed', {
    template: require('to-string-loader!./pb-video-feed.template.html'),
    controller: ['Video',
      function PbVideoFeedController(Video) {
        var self = this;

        Video.get().then(function(result) {
          self.videos = result.data.items;
        });
      }
    ]
  });