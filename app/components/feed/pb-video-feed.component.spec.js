describe('pbVideoFeed', function() {

  beforeEach(angular.mock.module('pbVideoFeed'));

  var $httpBackend, $rootScope, $compile, $templateCache, ctrl;

  beforeEach(inject(function($componentController, _$httpBackend_, _$rootScope_, _$compile_, _$templateCache_) {
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $templateCache = _$templateCache_;

    $httpBackend.expectGET('https://cdn.playbuzz.com/content/feed/items')
                .respond({items: [{"title": "Be a winner!"}, {"title": "How to prepare a great beer"}]});

    ctrl = $componentController('pbVideoFeed');
  }));

  describe('VideoFeedController', function() {
    it('should create a `videos` property with 2 videos fetched with `$http`', function() {
      jasmine.addCustomEqualityTester(angular.equals);

      expect(ctrl.videos).toBeUndefined();

      $httpBackend.flush();
      expect(ctrl.videos).toEqual([{"title": "Be a winner!"}, {"title": "How to prepare a great beer"}]);
    });
  });

  it('should render youtube component for youtube source', function() {
    $httpBackend.expectGET('https://cdn.playbuzz.com/content/feed/items')
                .respond({items: [{
                  "title": "Be a winner!","type": "video",
                  "source": "youtube",
                  "videoId": "I33u_EHLI3w",
                  "views": 12451409
                }]});

    $httpBackend.expectGET('https://content.googleapis.com/youtube/v3/videos?&id=I33u_EHLI3w&key=AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc&part=id,snippet,contentDetails,statistics')
                .respond({items: [{}]});

    var scope = $rootScope.$new();
    var element = angular.element('<pb-video-feed></pb-video-feed>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var youtubeComp = element.find('pb-youtube-item');

    expect(youtubeComp[0]).toBeDefined();
    expect(youtubeComp.length).toEqual(1);
  });

  it('should render html5 video component for url source', function() {
    $httpBackend.expectGET('https://cdn.playbuzz.com/content/feed/items')
                .respond({items: [{
                  "type": "video",
                  "source": "url",
                  "url": "http://cdn.playbuzz.com/content/feed/video-1.mp4",
                  "views": 8820
                }]});

    var scope = $rootScope.$new();
    var element = angular.element('<pb-video-feed></pb-video-feed>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var html5VideoComp = element.find('pb-video-item');

    expect(html5VideoComp[0]).toBeDefined();
    expect(html5VideoComp.length).toEqual(1);
  });

  it('should render facebook video component for facebook source', function() {
    $httpBackend.expectGET('https://cdn.playbuzz.com/content/feed/items')
                .respond({items: [{
                  "title": "How to prepare a great beer",
                  "type": "video",
                  "source": "facebook",
                  "videoId": "1052114818157758",
                  "views": 4569654
                }]});

    var scope = $rootScope.$new();
    var element = angular.element('<pb-video-feed></pb-video-feed>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var facebookVideoComp = element.find('pb-facebook-item');

    expect(facebookVideoComp[0]).toBeDefined();
    expect(facebookVideoComp.length).toEqual(1);
  });
});