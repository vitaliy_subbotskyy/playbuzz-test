import 'to-string-loader';
import './pb-video-item.module';

export default angular.
  module('pbVideoItem').
  component('pbVideoItem', {
    bindings: {
        video: '<'
    },
    template: require('to-string-loader!./pb-video-item.template.html')
  });