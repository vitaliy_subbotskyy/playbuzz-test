import 'to-string-loader';
import './pb-facebook-item.module';

export default angular.
  module('pbFacebookItem').
  component('pbFacebookItem', {
    bindings: {
        video: '<'
    },
    template: require('to-string-loader!./pb-facebook-item.template.html')
  });