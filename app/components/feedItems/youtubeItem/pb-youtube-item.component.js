import 'to-string-loader';
import './pb-youtube-item.module';

export default angular.
  module('pbYoutubeItem').
  component('pbYoutubeItem', {
    bindings: {
        video: '<'
    },
    template: require('to-string-loader!./pb-youtube-item.template.html'),
    controller: ['YoutubeInfo',
      function PbYoutubeItemController(YoutubeInfo) {
        var self = this;

        this.isVideoAvailable = true;

        this.$onChanges = function youTubeItemOnChanges(changedObj){
            var result = YoutubeInfo.getVideoById({
                videoId: changedObj.video.currentValue.videoId,
                key: "AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc",
            }).then(function (response) {
                self.isVideoAvailable = response.data.items.length;
            });
        }
      }
    ]
  });