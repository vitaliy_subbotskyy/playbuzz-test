describe('pbYoutubeItem', function() {

  beforeEach(angular.mock.module('pbYoutubeItem'));

  var $httpBackend, $rootScope, $compile, $templateCache, ctrl;

  beforeEach(inject(function($componentController, _$httpBackend_, _$rootScope_, _$compile_, _$templateCache_) {
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $templateCache = _$templateCache_;

    ctrl = $componentController('pbYoutubeItem');
  }));

  describe('pbYoutubeItemController', function() {
    it('should init with `isVideoAvailable` as true', function() {
      expect(ctrl.isVideoAvailable).toBe(true);
    });
  });

  it('should render youtube embeded video when items array is not empty', function() {
    var scope = $rootScope.$new();
    scope.video = { videoId: 'I33u_EHLI3w' };
    $httpBackend.expectGET('https://content.googleapis.com/youtube/v3/videos?&id=I33u_EHLI3w&key=AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc&part=id,snippet,contentDetails,statistics')
                .respond({items: [{}]});
    var element = angular.element('<pb-youtube-item video="video"></pb-youtube-item>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var videoElement = element[0].querySelectorAll('iframe');

    expect(videoElement[0]).toBeDefined();
  });

  it('should not render `not available` message when items array is not empty', function() {
    var scope = $rootScope.$new();
    scope.video = { videoId: 'I33u_EHLI3w' };
    $httpBackend.expectGET('https://content.googleapis.com/youtube/v3/videos?&id=I33u_EHLI3w&key=AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc&part=id,snippet,contentDetails,statistics')
                .respond({items: [{}]});
    var element = angular.element('<pb-youtube-item video="video"></pb-youtube-item>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var notAvailableMessage = element[0].querySelectorAll('.not-availalbe');

    expect(notAvailableMessage[0]).toBeUndefined();
  });

  it('should not render youtube embeded video when items array is empty', function() {
    var scope = $rootScope.$new();
    scope.video = { videoId: 'I33u_EHLI3w' };
    $httpBackend.expectGET('https://content.googleapis.com/youtube/v3/videos?&id=I33u_EHLI3w&key=AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc&part=id,snippet,contentDetails,statistics')
                .respond({items: []});
    var element = angular.element('<pb-youtube-item video="video"></pb-youtube-item>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var videoElement = element[0].querySelectorAll('iframe');

    expect(videoElement[0]).toBeUndefined();
  });

  it('should not render `not available` message when items array is empty', function() {
    var scope = $rootScope.$new();
    scope.video = { videoId: 'I33u_EHLI3w' };
    $httpBackend.expectGET('https://content.googleapis.com/youtube/v3/videos?&id=I33u_EHLI3w&key=AIzaSyDvrqL7P7U8YD4psfxeUQ8U0plzdHJOjbc&part=id,snippet,contentDetails,statistics')
                .respond({items: []});
    var element = angular.element('<pb-youtube-item video="video"></pb-youtube-item>');
    element = $compile(element)(scope);
    scope.$apply();
    $httpBackend.flush();

    var notAvailableMessage = element[0].querySelectorAll('.not-availalbe');

    expect(notAvailableMessage[0]).toBeDefined();
  });
});