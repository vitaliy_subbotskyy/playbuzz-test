import './core.module';

export default angular.
  module('core').
  filter('trustFacebook', ['$sce', function($sce) {
    return function(videoId) {
        return $sce.trustAsResourceUrl("https://www.facebook.com/video/embed?video_id=" + videoId);
    };
  }]);