import './core.module';

export default angular.
  module('core').
    filter('trustYoutube', ['$sce', function($sce) {
    return function(videoId) {
      return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + videoId);
    };
  }]);