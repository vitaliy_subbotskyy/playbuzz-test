import './core.module';

export default angular.
  module('core').
  filter('views', function() {
    return function(input) {
        if (input >= 1000000) {
            return (input / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        }
        if (input >= 1000) {
            return (input / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        }
        return input;
    };
  });