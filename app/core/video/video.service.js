import './video.module';

angular.
  module('core.video').
  factory('Video', ['$http',
    function videoFactory($http) {
      return {
        get: function() {
          return $http({
            method: 'GET',
            url: 'https://cdn.playbuzz.com/content/feed/items'
          });
        }
      }
    }
  ]);