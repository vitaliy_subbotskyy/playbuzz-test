import './video.module';

angular.
  module('core.video').
  factory('YoutubeInfo', ['$http', function ($http) {
    var youtubeInfoFactory = {};

    youtubeInfoFactory.getVideoById = function (params) {
      var youtubeItemData = {
        key: params.key,
        part: "id,snippet,contentDetails,statistics",
        id: params.videoId
      };

      return $http({
        method: 'GET',
        url: "https://content.googleapis.com/youtube/v3/videos?",
        params: youtubeItemData,
      });
    };

    return youtubeInfoFactory;
  }]);