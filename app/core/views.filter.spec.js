describe('views filter', function() {

    beforeEach(angular.mock.module('core'));

    it('should convert values over 1000 to <number>K representation',
        inject(function(viewsFilter) {
            expect(viewsFilter(254999)).toBe('255K');
        })
    );

    it('should include one number after decimal point and round it to nearest whole',
        inject(function(viewsFilter) {
            expect(viewsFilter(254199)).toBe('254.2K');
        })
    );

    it('shouldn\'t include number after decimal if that number is zero',
        inject(function(viewsFilter) {
            expect(viewsFilter(254001)).toBe('254K');
        })
    );

    it('should convert values over 1000000 to <number>M representation',
        inject(function(viewsFilter) {
            expect(viewsFilter(254999999)).toBe('255M');
        })
    );
});