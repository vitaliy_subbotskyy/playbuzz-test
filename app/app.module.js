import styles from './app.css';
import styles from './app.animations.css';

import angular from 'angular';
import 'angular-animate';

import title from './components/title';
import videoFeed from './components/feed';

angular.module('bpTestApp', [
  'ngAnimate',
  'pbTitle',
  'pbVideoFeed'
]).config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
  // that is for tools like protractor not used here, so turning it off
  // https://code.angularjs.org/1.5.5/docs/guide/production
}]);;